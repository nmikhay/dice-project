import random
from flask import Flask, render_template, request, redirect, url_for, session

app = Flask(__name__)
app.secret_key = "my_secret_key"


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        player1 = request.form.get("player1", None)
        player2 = request.form.get("player2", None)

        if player1 is not None and player2 is not None:
            session["player1"] = player1
            session["player2"] = player2
            session["score1"] = 0
            session["score2"] = 0
            session["running_score"] = 0
            session["current_player"] = 1
            return redirect(url_for("game"))

    return render_template("index.html")

@app.route("/game", methods=["GET", "POST"])
def game():
    dice_image = None
    winner = None

    if request.method == "POST":
        action = request.form.get("action")

        if action == "roll":
            roll = roll_dice()
            dice_image = f"/static/images/die{roll}.bmp"
            if roll == 1:
                session["running_score"] = 0
                swap_turn()
            else:
                session["running_score"] += roll
        elif action == "hold":
            update_score()
            swap_turn()

        winner = check_winner()

    return render_template("game.html", dice_image=dice_image, winner=winner)

def roll_dice():
    return random.randint(1, 6)


def swap_turn():
    if session["current_player"] == 1:
        session["current_player"] = 2
    else:
        session["current_player"] = 1
    session["running_score"] = 0


def update_score():
    if session["current_player"] == 1:
        session["score1"] += session["running_score"]
    else:
        session["score2"] += session["running_score"]


def check_winner():
    if session["score1"] >= 20:
        return session["player1"]
    elif session["score2"] >= 20:
        return session["player2"]
    else:
        return None

@app.route("/reset_game", methods=["POST"])
def reset_game():
    session["score1"] = 0
    session["score2"] = 0
    session["running_score"] = 0
    session["current_player"] = 1
    return redirect(url_for("game"))


if __name__ == "__main__":
    app.run(debug=True)

